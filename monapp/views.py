from django.shortcuts import render
from django.http import HttpResponse
from .models import Phrasedansbdd
from django.template import loader

# Create your views here.
 
def index(request):
    phrases = Phrasedansbdd.objects.order_by('id')
    context = {'phrases': phrases}
    return render(request, 'monapp/index.html', context)

# def index(request):
#     phrases = Phrasedansbdd.objects.order_by('id')
#     template = loader.get_template('monapp/index.html')
#     context = {'phrases': phrases}
#     return HttpResponse(template.render(context, request))
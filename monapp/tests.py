from django.test import TestCase
from .models import Phrasedansbdd
from django.urls import reverse

# Create your tests here.
def create_Phrasedansbdd(nom):
    phrasedansbdd = Phrasedansbdd(nom=nom)
    phrasedansbdd.save()
    return phrasedansbdd

class test(TestCase):
    def test_creation_Phrasedansbdd(self):
        nom = "Phrase de test"
        phrase = create_Phrasedansbdd(nom)
        self.assertEqual(phrase.nom, nom)

    def test_iteration_Phrasedansbdd_pageindex(self):
        nom = "Phrase de test"
        phrase = create_Phrasedansbdd(nom)

        nom2 = "Phrase de test2"
        phrase2 = create_Phrasedansbdd(nom2)

        response = self.client.get(reverse('monapp:index'))
        self.assertContains(response, phrase)
        self.assertContains(response, phrase2)